package fr.cloud4you.pa1007.economy_utilities.creator;

import java.util.Objects;
import java.util.UUID;

public class Account {


    /**
     * the balance of the account.
     *
     * @since 0.1
     */
    private long accountBalance;
    /**
     * the uuid of the player.
     *
     * @since 0.1
     */
    private UUID playerUUID;

    public Account(long accountBalance, UUID playerUUID) {
        this.accountBalance = accountBalance;
        this.playerUUID = playerUUID;
    }

    /**
     * @return the balance of the account.
     * @since 0.1
     */
    public long getAccountBalance() {
        return this.accountBalance;
    }

    /**
     * Sets the <code>accountBalance</code> field.
     *
     * @param AccountBalance the balance of the account.
     * @since 0.1
     */
    public void setAccountBalance(long AccountBalance) {
        this.accountBalance = AccountBalance;
    }

    /**
     * @return the uuid of the player.
     * @since 0.1
     */
    public UUID getPlayerUUID() {
        return this.playerUUID;
    }

    /**
     * Sets the <code>playerUUID</code> field.
     *
     * @param playerUUID the uuid of the player.
     * @since 0.1
     */
    public void setPlayerUUID(UUID playerUUID) {
        this.playerUUID = playerUUID;
    }

    public long addToAccount(long plus) {

        accountBalance = accountBalance + plus;
        return accountBalance;

    }

    public long decreaseAccount(long minus) {

        accountBalance = accountBalance - minus;
        return accountBalance;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Account)) {
            return false;
        }
        Account account = (Account) o;
        return accountBalance == account.accountBalance &&
               Objects.equals(playerUUID, account.playerUUID);
    }

    @Override
    public int hashCode() {

        return Objects.hash(accountBalance, playerUUID);
    }

    @Override
    public String toString() {
        return com.google.common.base.MoreObjects.toStringHelper(this)
                .add("accountBalance", accountBalance)
                .add("playerUUID", playerUUID)
                .toString();
    }

}
