package fr.cloud4you.pa1007.economy_utilities.constants;

import java.util.UUID;

public final class Constants {


    /**
     * Plugin ID Constants .
     */
    public static final String ID = "economy_utilities";

    /**
     * Plugin DESCRIPTION Constants .
     */
    public static final String DESCRIPTION = "This plugin is for money integration with deals for DepthDarkCity server";

    /**
     * Plugin URL Constants .
     */
    public static final String URL = "http://pa1007.cloud4you.fr/";

    /**
     * Plugin VERSION Constants .
     */
    public static final String VERSION = "0.1";

    /**
     * Plugin NAME Constants .
     */
    public static final String NAME = "EconomyUtilities";

    /**
     * Plugin Main AUTHOR Constants .
     */
    public static final String MAIN_AUTHOR = "pa1007";

    /**
     * get all the data from the table
     */
    public static final String DATA_BASE_MAIN = "SELECT * FROM ``"; // add the table name like : `Balance`

    /**
     * Command base without the condition
     */
    private static final String DATA_BASE_DELETE_ROW = "DELETE FROM ``"; // add the table name like : `Balance`

    /**
     * DataBase base URL .
     */
    private static final String DATA_BASE_URL = "jdbc:mysql://";    //for mysql connection

    /**
     * DataBase HOST
     */
    private static final String DATA_BASE_HOST = ""; // add  host:port/databaseName like : pa1007.me:1234/DataBaseName


    /**
     * DataBase User .
     */
    private static final String DATA_BASE_USER = "minecraft";   //User for the database change

    /**
     * DataBase password .
     */
    private static final String DATA_BASE_PASSWORD = ""; // user password

    /**
     * Create new account
     */
    private static final String QUERY_CREATE_ACCOUNT =
            "INSERT INTO  `?`.`??` (`Balance` ,`UUID` ,`lastChange`)VALUES ('";  // ?-> database name /??-> table name

    /**
     * Main Update balance
     */
    private static final String DATA_UPDATE = "UPDATE ?? SET Balance = '"; //??-> table name

    /**
     * The URL of the minecraft database
     *
     * @return SQL command
     */
    public static String getDataBaseUrl() {
        return DATA_BASE_URL + DATA_BASE_USER + ":" + DATA_BASE_PASSWORD + "@" + DATA_BASE_HOST;
    }

    /**
     * The URL for update money
     *
     * @param money for the player
     * @param uuid  Player UUID
     * @return SQL command
     */
    public static String getUpdateURLBalance(long money, UUID uuid) {
        return DATA_UPDATE + money + "', lastChange = NOW() WHERE UUID = '" + uuid + "';";
    }

    /**
     * DataBase command to get the account balance.
     *
     * @param uuid UUID of the player
     * @return SQL command
     */
    public static String getUserMoney(UUID uuid) {
        return DATA_BASE_MAIN + " WHERE UUID = '" + uuid + "';";
    }

    /**
     * DataBase command to delete user row.
     *
     * @param uuid UUID of the player
     * @return SQL command
     */
    public static String getDeleteRow(UUID uuid) {
        return DATA_BASE_DELETE_ROW + " WHERE UUID = '" + uuid + "';";
    }

    /**
     * to create a new account !
     *
     * @param money the money set for the new player
     * @param uuid  the UUID of the player
     * @return SQL command
     */
    public static String createNewAccount(long money, UUID uuid) {
        return QUERY_CREATE_ACCOUNT + money + "',  '" + uuid + " ',NOW() );";
    }
}
