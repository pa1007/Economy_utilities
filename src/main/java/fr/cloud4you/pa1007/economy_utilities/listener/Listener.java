package fr.cloud4you.pa1007.economy_utilities.listener;

public interface Listener {
    /**
     * get the name of the event
     *
     * @return String (name of the event)
     */
    String getEventName();
}