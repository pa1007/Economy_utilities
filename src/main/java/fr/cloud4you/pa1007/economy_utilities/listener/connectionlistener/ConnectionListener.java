package fr.cloud4you.pa1007.economy_utilities.listener.connectionlistener;

import fr.cloud4you.pa1007.economy_utilities.DataBaseConnection;
import fr.cloud4you.pa1007.economy_utilities.Economy_Utilities;
import fr.cloud4you.pa1007.economy_utilities.constants.Constants;
import fr.cloud4you.pa1007.economy_utilities.creator.Account;
import fr.cloud4you.pa1007.economy_utilities.listener.AbstractListener;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionListener extends AbstractListener {

    private DataBaseConnection dbConnection = new DataBaseConnection();
    private Connection         connection;
    private long               moneyStart   = 100L;

    public ConnectionListener(Economy_Utilities economy_utilities) {
        super(economy_utilities);
    }

    @Override
    public String getEventName() {
        return "Connection";
    }

    @Listener(order = Order.FIRST)
    public void onPlayerConnect(ClientConnectionEvent.Join event) throws SQLException {
        Player player = event.getTargetEntity();
        if (!pluginInstance.getAccountMap().containsKey(player.getUniqueId())) {
            System.out.println("\n new Connected");
            connection = dbConnection.getConnection();
            connection.prepareStatement(Constants.createNewAccount(
                    moneyStart,
                    event.getTargetEntity().getUniqueId()
            )).execute();
            connection.close();
            pluginInstance.getAccountMap().put(player.getUniqueId(), new Account(moneyStart, player.getUniqueId()));
            pluginInstance.saveAccount();
        }
        else {
            pluginInstance.getLogger().info(
                    "Player {} has successfully join the server",
                    event.getTargetEntity().getName()
            );
        }
    }
}
