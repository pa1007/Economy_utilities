package fr.cloud4you.pa1007.economy_utilities.listener;

import fr.cloud4you.pa1007.economy_utilities.Economy_Utilities;
import java.util.Objects;

public abstract class AbstractListener implements Listener {

    protected final Economy_Utilities pluginInstance;

    public AbstractListener(Economy_Utilities economy_utilities) {
        Objects.requireNonNull(economy_utilities);

        this.pluginInstance = economy_utilities;
    }
}
