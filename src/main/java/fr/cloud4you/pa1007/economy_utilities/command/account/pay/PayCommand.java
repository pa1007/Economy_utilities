package fr.cloud4you.pa1007.economy_utilities.command.account.pay;

import fr.cloud4you.pa1007.economy_utilities.Economy_Utilities;
import fr.cloud4you.pa1007.economy_utilities.Permissions;
import fr.cloud4you.pa1007.economy_utilities.command.AbstractCommand;
import fr.depthdarkcity.sponge_utilitises.creator.CommonException;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class PayCommand extends AbstractCommand {

    public PayCommand(Economy_Utilities economy_Utilities) {
        super(economy_Utilities);
    }

    @Override
    public String[] getNames() {
        return new String[]{"pay"};
    }

    @Override
    public CommandSpec createCommand() {
        return CommandSpec.builder()
                .arguments(
                        GenericArguments.player(Text.of("Player")),
                        GenericArguments.longNum(Text.of("Money"))
                )
                .permission(Permissions.ACCOUNT_PAY)
                .description(Text.of("to pay somme one "))
                .executor(this)
                .build();
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        if (!(src instanceof Player)) {
            throw new CommandException(CommonException.CONSOLE_SOURCE_EXCEPTION);
        }
        Player player = (Player) src;
        Player seller = args.<Player>getOne(Text.of("Player")).get();
        long   money  = args.<Long>getOne(Text.of("Money")).get();

        if (money <= 0) {
            throw new CommandException(Text.of("You have set an unavailable amount! must be positive !"));
        }
        if (player.getUniqueId() == seller.getUniqueId()) {
            throw new CommandException(Text.of("You cannot pay yourself"));
        }

        boolean done = pluginInstance.payToOtherPlayer(player, money, seller);

        if (done) {
            player.sendMessage(Text.of(
                    "You have been debited of ",
                    TextColors.BLUE,
                    "$" + money,
                    TextColors.RESET,
                    " to pay the player : " + seller.getName()
            ));

            seller.sendMessage(Text.of(
                    "You account has been credited of ",
                    TextColors.BLUE,
                    "$" + money,
                    TextColors.RESET,
                    " from the player : " + player.getName()
            ));
            pluginInstance.getLogger().trace("Transaction between the player "
                                             + player.getUniqueId()
                                             + " and the seller "
                                             + seller.getUniqueId()
                                             + " for $ "
                                             + money);


        }
        else {
            throw new CommandException(Text.of(
                    "unable to do the transaction, please come to discord and report you issue with the command line"));
        }
        return CommandResult.success();

    }
}
