package fr.cloud4you.pa1007.economy_utilities.command;

import fr.cloud4you.pa1007.economy_utilities.Economy_Utilities;
import java.util.Objects;

public abstract class AbstractCommand implements Command {

    protected final Economy_Utilities pluginInstance;

    public AbstractCommand(Economy_Utilities economy_utilities) {
        Objects.requireNonNull(economy_utilities);

        this.pluginInstance = economy_utilities;
    }
}