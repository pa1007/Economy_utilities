package fr.cloud4you.pa1007.economy_utilities.command.database;

import fr.cloud4you.pa1007.economy_utilities.Economy_Utilities;
import fr.cloud4you.pa1007.economy_utilities.command.AbstractCommand;
import fr.cloud4you.pa1007.economy_utilities.command.Command;
import fr.cloud4you.pa1007.economy_utilities.command.database.databasesubvommand.TestSubCommand;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;

public class DataBaseCommand extends AbstractCommand {

    private final Command testCommand;

    public DataBaseCommand(Economy_Utilities economy_utilities) {
        super(economy_utilities);
        testCommand = new TestSubCommand(economy_utilities);
    }

    @Override
    public String[] getNames() {
        return new String[]{"database"};
    }

    @Override
    public CommandSpec createCommand() {
        return CommandSpec.builder()
                .child(this.testCommand.createCommand(), this.testCommand.getNames())
                .description(Text.of("main command for database"))
                .executor(this)
                .build();
    }

    @Override
    public CommandResult execute(
            CommandSource src, CommandContext args
    ) {
        return CommandResult.empty();

    }
}
