package fr.cloud4you.pa1007.economy_utilities.command.account.balance;

import fr.cloud4you.pa1007.economy_utilities.DataBaseConnection;
import fr.cloud4you.pa1007.economy_utilities.Economy_Utilities;
import fr.cloud4you.pa1007.economy_utilities.Permissions;
import fr.cloud4you.pa1007.economy_utilities.command.AbstractCommand;
import fr.cloud4you.pa1007.economy_utilities.command.Command;
import fr.cloud4you.pa1007.economy_utilities.command.account.balance.adminsubcommand.BalanceSubCommand;
import fr.cloud4you.pa1007.economy_utilities.command.account.balance.adminsubcommand.EditSubCommand;
import fr.cloud4you.pa1007.economy_utilities.command.account.balance.adminsubcommand.WarningSubCommand;
import fr.cloud4you.pa1007.economy_utilities.constants.Constants;
import fr.depthdarkcity.sponge_utilities.SpongeUtilities;
import fr.depthdarkcity.sponge_utilities.creator.CommonException;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountBalanceCommand extends AbstractCommand {

    private       DataBaseConnection dbConnection = new DataBaseConnection();
    private       Connection         connection;
    private final Command            editCommand, balanceCommand, warningCommand;

    public AccountBalanceCommand(Economy_Utilities economy_Utilities) {
        super(economy_Utilities);
        warningCommand = new WarningSubCommand(economy_Utilities);
        editCommand = new EditSubCommand(economy_Utilities);
        balanceCommand = new BalanceSubCommand(economy_Utilities);
    }

    @Override
    public String[] getNames() {
        return new String[]{"account"};
    }

    @Override
    public CommandSpec createCommand() {
        return CommandSpec.builder()
                .child(this.warningCommand.createCommand(), this.warningCommand.getNames())
                .child(this.editCommand.createCommand(), this.editCommand.getNames())
                .child(this.balanceCommand.createCommand(), this.balanceCommand.getNames())
                .permission(Permissions.ACCOUNT_BALANCE)
                .description(Text.of("command to get yours balance"))
                .executor(this)
                .build();
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        SpongeUtilities   spongeUtilities = SpongeUtilities.getPluginInstance();
        PreparedStatement stmt;
        ResultSet         results;
        if (!(src instanceof Player)) {
            throw new CommandException(CommonException.CONSOLE_SOURCE_EXCEPTION);
        }

        Player player = (Player) src;

        if (src.hasPermission(Permissions.ACCOUNT_ADMIN)) {

            return CommandResult.empty();
        }
        else {
            try {
                connection = dbConnection.getConnection();
                stmt = connection.prepareStatement(Constants.getUserMoney(player.getUniqueId()));
                results = stmt.executeQuery();
            }
            catch (SQLException e) {
                e.printStackTrace();
                if (spongeUtilities.getDebugList().contains(player.getUniqueId())) {
                    spongeUtilities.debugInChatMessage(player, Text.of(e.getLocalizedMessage()));
                }
                return CommandResult.empty();
            }
            long money;
            try {
                results.first();
                money = results.getLong("Balance");
                src.sendMessage(Text.of(
                        "Your account balance is ",
                        TextColors.BLUE,
                        "$" + money,
                        TextColors.RESET,
                        " !"
                ));
                return CommandResult.success();
            }
            catch (SQLException e) {
                e.printStackTrace();
                if (spongeUtilities.getDebugList().contains(player.getUniqueId())) {
                    spongeUtilities.debugInChatMessage(player, Text.of(e.getLocalizedMessage()));
                }
                return CommandResult.empty();
            }


        }


    }
}
