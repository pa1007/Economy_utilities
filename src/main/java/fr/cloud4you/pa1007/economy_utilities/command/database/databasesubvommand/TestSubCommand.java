package fr.cloud4you.pa1007.economy_utilities.command.database.databasesubvommand;

import fr.cloud4you.pa1007.economy_utilities.DataBaseConnection;
import fr.cloud4you.pa1007.economy_utilities.Economy_Utilities;
import fr.cloud4you.pa1007.economy_utilities.Permissions;
import fr.cloud4you.pa1007.economy_utilities.command.AbstractCommand;
import fr.depthdarkcity.sponge_utilities.SpongeUtilities;
import fr.depthdarkcity.sponge_utilities.creator.CommonException;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import java.sql.Connection;
import java.sql.SQLException;

public class TestSubCommand extends AbstractCommand {


    public TestSubCommand(Economy_Utilities economy_Utilities) {
        super(economy_Utilities);
    }

    @Override
    public String[] getNames() {
        return new String[]{"test"};
    }

    @Override
    public CommandSpec createCommand() {
        return CommandSpec.builder()
                .arguments()
                .permission(Permissions.DATABASE_ADMIN)
                .description(Text.of("testing the connection to the database"))
                .executor(this)
                .build();
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        SpongeUtilities    spongeUtilities = SpongeUtilities.getPluginInstance();
        DataBaseConnection dbConnection    = new DataBaseConnection();
        if (!(src instanceof Player)) {
            throw new CommandException(CommonException.CONSOLE_SOURCE_EXCEPTION);
        }
        Player player = (Player) src;

        try {
            Connection connection = dbConnection.getConnection();
            if (connection.isValid(1)) {
                src.sendMessage(Text.of("The server is able to connect to the database !"));
                return CommandResult.success();
            }
            else {
                throw new CommandException(Text.of("The server can't connect to the database"));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
            if (spongeUtilities.getDebugList().contains(player.getUniqueId())) {
                spongeUtilities.debugInChatMessage(player, Text.of(e.getLocalizedMessage()));
            }
            throw new CommandException(Text.of("The server can't connect to the database"));
        }
    }
}
