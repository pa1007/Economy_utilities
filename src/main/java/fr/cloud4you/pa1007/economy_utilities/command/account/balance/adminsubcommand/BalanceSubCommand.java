package fr.cloud4you.pa1007.economy_utilities.command.account.balance.adminsubcommand;

import fr.cloud4you.pa1007.economy_utilities.DataBaseConnection;
import fr.cloud4you.pa1007.economy_utilities.Economy_Utilities;
import fr.cloud4you.pa1007.economy_utilities.Permissions;
import fr.cloud4you.pa1007.economy_utilities.command.AbstractCommand;
import fr.cloud4you.pa1007.economy_utilities.constants.Constants;
import fr.depthdarkcity.sponge_utilities.SpongeUtilities;
import fr.depthdarkcity.sponge_utilities.creator.CommonException;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Identifiable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.UUID;

public class BalanceSubCommand extends AbstractCommand {

    private DataBaseConnection dbConnection    = new DataBaseConnection();
    private Connection         connection;
    private SpongeUtilities    spongeUtilities = SpongeUtilities.getPluginInstance();

    public BalanceSubCommand(Economy_Utilities economy_utilities) {
        super(economy_utilities);
    }

    @Override
    public String[] getNames() {
        return new String[]{"balance"};
    }

    @Override
    public CommandSpec createCommand() {
        return CommandSpec.builder()
                .arguments(GenericArguments.optional(GenericArguments.player(Text.of("Player"))))
                .permission(Permissions.ACCOUNT_ADMIN)
                .description(Text.of("balance command for the admin"))
                .executor(this)
                .build();
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        PreparedStatement stmt;
        ResultSet         results;
        if (!(src instanceof Player)) {
            throw new CommandException(CommonException.CONSOLE_SOURCE_EXCEPTION);
        }
        Player           player      = (Player) src;
        Optional<Player> givenPlayer = args.getOne(Text.of("Player"));
        UUID             usedUUID    = givenPlayer.map(Identifiable::getUniqueId).orElseGet(player::getUniqueId);

        try {
            connection = dbConnection.getConnection();
            stmt = connection.prepareStatement(Constants.getUserMoney(usedUUID));
            results = stmt.executeQuery();
        }
        catch (SQLException e) {
            e.printStackTrace();
            if (spongeUtilities.getDebugList().contains(player.getUniqueId())) {
                spongeUtilities.debugInChatMessage(player, Text.of(e.getLocalizedMessage()));
            }
            return CommandResult.empty();
        }
        long money;
        try {
            results.first();
            money = results.getLong("Balance");
            src.sendMessage(Text.of(
                    "The account balance is ",
                    TextColors.BLUE,
                    "$" + money,
                    TextColors.RESET,
                    " for the player : " + Sponge.getServer().getPlayer(usedUUID).get().getName()
            ));
            if (!pluginInstance.getBalanceMap().containsKey(usedUUID)) {
                pluginInstance.getBalanceMap().put(usedUUID, money);
            }
            else {
                if (!pluginInstance.getBalanceMap().get(usedUUID).equals(money)) {
                    pluginInstance.getBalanceMap().replace(usedUUID, money);
                }
            }
            return CommandResult.success();
        }
        catch (SQLException e) {
            e.printStackTrace();
            if (spongeUtilities.getDebugList().contains(player.getUniqueId())) {
                spongeUtilities.debugInChatMessage(player, Text.of(e.getLocalizedMessage()));
            }
            return CommandResult.empty();
        }
    }
}
