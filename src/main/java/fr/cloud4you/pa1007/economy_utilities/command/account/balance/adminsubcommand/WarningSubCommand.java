package fr.cloud4you.pa1007.economy_utilities.command.account.balance.adminsubcommand;

import fr.cloud4you.pa1007.economy_utilities.Economy_Utilities;
import fr.cloud4you.pa1007.economy_utilities.Permissions;
import fr.cloud4you.pa1007.economy_utilities.command.AbstractCommand;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;

public class WarningSubCommand extends AbstractCommand {


    public WarningSubCommand(Economy_Utilities economy_utilities) {
        super(economy_utilities);
    }

    @Override
    public String[] getNames() {
        return new String[]{"warning"};
    }

    @Override
    public CommandSpec createCommand() {
        return CommandSpec.builder()
                .arguments()
                .permission(Permissions.ACCOUNT_ADMIN)
                .description(Text.of("The warning command"))
                .executor(this)
                .build();
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) {
        return null;
    }
}
