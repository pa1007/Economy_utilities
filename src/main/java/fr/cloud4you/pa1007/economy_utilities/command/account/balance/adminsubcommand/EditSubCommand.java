package fr.cloud4you.pa1007.economy_utilities.command.account.balance.adminsubcommand;

import fr.cloud4you.pa1007.economy_utilities.DataBaseConnection;
import fr.cloud4you.pa1007.economy_utilities.Economy_Utilities;
import fr.cloud4you.pa1007.economy_utilities.Permissions;
import fr.cloud4you.pa1007.economy_utilities.command.AbstractCommand;
import fr.cloud4you.pa1007.economy_utilities.constants.Constants;
import fr.depthdarkcity.sponge_utilities.SpongeUtilities;
import fr.depthdarkcity.sponge_utilities.creator.CommonException;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;
import java.util.UUID;

public class EditSubCommand extends AbstractCommand {

    private DataBaseConnection dbConnection    = new DataBaseConnection();
    private Connection         connection;
    private SpongeUtilities    spongeUtilities = SpongeUtilities.getPluginInstance();

    public EditSubCommand(Economy_Utilities economy_utilities) {
        super(economy_utilities);
    }

    @Override
    public String[] getNames() {
        return new String[]{"edit"};
    }

    @Override
    public CommandSpec createCommand() {
        return CommandSpec.builder()
                .arguments(
                        GenericArguments.longNum(Text.of("Money")),
                        GenericArguments.optional(GenericArguments.player(Text.of("Player"))),
                        GenericArguments.optional(GenericArguments.uuid(Text.of("UUID")))
                )
                .permission(Permissions.ACCOUNT_ADMIN)
                .description(Text.of("The edit command for the balance"))
                .executor(this)
                .build();
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        if (!(src instanceof Player)) {
            throw new CommandException(CommonException.CONSOLE_SOURCE_EXCEPTION);
        }
        Player           player         = (Player) src;
        Player           changePlayer;
        Optional<Player> optionalPlayer = args.getOne(Text.of("Player"));
        Optional<UUID>   optionalUuid   = args.getOne(Text.of("UUID"));
        long             money          = args.<Long>getOne(Text.of("Money")).get();
        if (money < 0L) {
            throw new CommandException(Text.of("You can't set an negative value"));
        }
        if (optionalPlayer.isPresent()) {
            changePlayer = optionalPlayer.get();
        }
        else if (optionalUuid.isPresent()) {
            Optional<Player> optionalPlayer2 = Sponge.getServer().getPlayer(optionalUuid.get());
            if (optionalPlayer2.isPresent()) {
                changePlayer = Sponge.getServer().getPlayer(optionalUuid.get()).get();
            }
            else {
                throw new CommandException(Text.of("The UUID you have set is not register"));
            }
        }
        else {
            throw new CommandException(Text.of("You must put a player or an valid UUID "));
        }
        try {
            connection = dbConnection.getConnection();
            Boolean done2 = connection.prepareStatement(Constants.getDeleteRow(changePlayer.getUniqueId()))
                    .execute();
            if (done2) {
                pluginInstance.getLogger().debug("Delete successfully");
            }
            else {
                pluginInstance.getLogger().debug("Delete wasn't successfully");
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
            if (spongeUtilities.getDebugList().contains(player.getUniqueId())) {
                spongeUtilities.debugInChatMessage(player, Text.of(e.getLocalizedMessage()));
            }
            return CommandResult.empty();
        }
        try {
            boolean done = connection.prepareStatement(Constants.createNewAccount(
                    money,
                    changePlayer.getUniqueId()
            )).execute();

            src.sendMessage(Text.of(
                    "The account balance is ",
                    TextColors.BLUE,
                    "$" + money,
                    TextColors.RESET,
                    " for the player : " + changePlayer.getName()
            ));
            return CommandResult.success();


        }
        catch (SQLException e) {
            e.printStackTrace();
            if (spongeUtilities.getDebugList().contains(player.getUniqueId())) {
                spongeUtilities.debugInChatMessage(player, Text.of(e.getLocalizedMessage()));
            }
            return CommandResult.empty();
        }

    }
}
