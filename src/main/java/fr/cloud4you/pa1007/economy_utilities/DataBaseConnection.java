package fr.cloud4you.pa1007.economy_utilities;

import fr.cloud4you.pa1007.economy_utilities.constants.Constants;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.service.sql.SqlService;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class DataBaseConnection {

    private Connection        connection;
    private boolean           connected;
    private SqlService        sql;
    private Economy_Utilities pluginInstance = Economy_Utilities.getPluginInstance();

    public Connection getConnection() throws SQLException {
        if (connected && connection.isValid(1)) {

            return connection;
        }
        else {
            try {
                connection = getDataSource(Constants.getDataBaseUrl()).getConnection();
                connected = true;
                return connection;
            }
            catch (Exception e) {
                e.printStackTrace();
                return null;

            }
        }
    }


    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    private DataSource getDataSource(String jdbcUrl) throws SQLException {
        if (sql == null) {
            sql = Sponge.getServiceManager().provide(SqlService.class).get();
        }
        return sql.getDataSource(
                pluginInstance,
                jdbcUrl
        );
    }
}
