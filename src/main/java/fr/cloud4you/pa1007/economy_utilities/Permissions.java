package fr.cloud4you.pa1007.economy_utilities;

public final class Permissions {

    /**
     * Permission for getting the account balance.
     */
    public static final String ACCOUNT_BALANCE = "fr.cloud4you.pa1007.economy_utilities.account_balance";

    /**
     * Permission for been an admin of the balance.
     */
    public static final String ACCOUNT_ADMIN = "fr.cloud4you.pa1007.economy_utilities.account_admin";

    /**
     * Permission for the admin of the database.
     */
    public static final String DATABASE_ADMIN = "fr.cloud4you.pa1007.economy_utilities.database_admin";

    /**
     * Permission for paying somme one.
     */
    public static final String ACCOUNT_PAY = "fr.cloud4you.pa1007.economy_utilities.account_pay";
}
