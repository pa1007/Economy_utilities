package fr.cloud4you.pa1007.economy_utilities;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.cloud4you.pa1007.economy_utilities.command.Command;
import fr.cloud4you.pa1007.economy_utilities.command.account.balance.AccountBalanceCommand;
import fr.cloud4you.pa1007.economy_utilities.command.account.pay.PayCommand;
import fr.cloud4you.pa1007.economy_utilities.command.database.DataBaseCommand;
import fr.cloud4you.pa1007.economy_utilities.constants.Constants;
import fr.cloud4you.pa1007.economy_utilities.creator.Account;
import fr.cloud4you.pa1007.economy_utilities.listener.Listener;
import fr.cloud4you.pa1007.economy_utilities.listener.connectionlistener.ConnectionListener;
import fr.depthdarkcity.sponge_utilities.SpongeUtilities;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.config.ConfigDir;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.game.GameReloadEvent;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.plugin.Dependency;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import javax.inject.Inject;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@Plugin(id = Constants.ID,
        name = Constants.NAME,
        description = Constants.DESCRIPTION,
        version = Constants.VERSION,
        url = Constants.URL,
        authors = {Constants.MAIN_AUTHOR},
        dependencies = {@Dependency(id = "sponge_utilities")})
public class Economy_Utilities {

    private static final Gson               GSON = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
    private static       Economy_Utilities  pluginInstance;
    private final        Command[]          commands;
    private              SpongeUtilities    spongeUtilities;
    private final        Listener[]         events;
    private final        Map<UUID, Account> accountMap;
    private final        Map<UUID, Long>    tempBalanceMap;
    @Inject
    private              Logger             logger;
    @Inject
    @ConfigDir(sharedRoot = false)
    private              Path               configPath;

    public Economy_Utilities() {
        this.commands = new Command[]{
                new AccountBalanceCommand(this),
                new DataBaseCommand(this),
                new PayCommand(this)
        };
        this.events = new Listener[]{
                new ConnectionListener(this)
        };
        accountMap = new HashMap<>();
        tempBalanceMap = new HashMap<>();
    }

    @org.spongepowered.api.event.Listener
    public void preInit(GamePreInitializationEvent evt) {
        loadAccount();
        try {
            spongeUtilities = SpongeUtilities.getPluginInstance();
            logger.debug("The plugin SpongeUtilities is installed");
        }
        catch (NoClassDefFoundError | NullPointerException e) {
            logger.error("The plugin SpongeUtilities is not Installed ");
        }
        this.logger.debug("Registering commands...");
        for (Command command : this.commands) {
            this.logger.trace("Registering command {}", String.join("/", command.getNames()));

            Sponge.getCommandManager().register(this, command.createCommand(), command.getNames());
        }
        for (Listener event : this.events) {
            this.logger.trace("Registering event {}", String.join("/", event.getEventName()));

            Sponge.getEventManager().registerListeners(this, event);
        }

        this.logger.debug("Registered {} commands.", this.commands.length);
        this.logger.debug("Registered {} events.", this.events.length);


    }

    @org.spongepowered.api.event.Listener
    public void reload(GameReloadEvent event) {
        saveAccount();
        loadAccount();

    }

    public Map<UUID, Account> getAccountMap() {
        return accountMap;
    }

    public Logger getLogger() {
        return logger;
    }

    public void saveAccount() {
        Path accountFile = this.getAccountFile();

        try {
            this.logger.info("Saving accountMap...");

            Path parentDir = accountFile.getParent();

            if (!Files.isDirectory(parentDir)) {
                Files.createDirectories(parentDir);
            }

            try (Writer writer = Files.newBufferedWriter(accountFile)) {
                GSON.toJson(this.accountMap.values(), writer);
            }

            this.logger.info("Successfully saved {} accountMap!", this.accountMap.size());
        }
        catch (IOException e) {
            this.logger.error("Unable to save accountMap:", e);
        }
    }

    public void loadAccount() {
        Path accountFile = this.getAccountFile();

        this.logger.info("Loading accountMap...");

        if (Files.notExists(accountFile)) {
            this.logger.info("No warp loaded since the file containing the accountMap does not exists!");
            return;
        }

        try (Reader reader = Files.newBufferedReader(accountFile)) {
            Account[] warps = GSON.fromJson(reader, Account[].class);

            this.accountMap.putAll(Arrays.stream(warps).collect(Collectors.toMap(
                    Account::getPlayerUUID,
                    Function.identity()
            )));
        }
        catch (IOException e) {
            this.logger.error("Unable to load accountMap:", e);
        }
    }

    /**
     * Money cannot been negative , condition check before
     * to get the action done , use this method
     *
     * @param player player to give the money
     * @param money  the amount tha the player give an the seller will have
     * @param seller the seller who will receive money
     * @return Map with 2 result , the uuid of the person and the money lose/earn
     */
    public boolean payToOtherPlayer(Player player, long money, Player seller) {
        DataBaseConnection dbConnection = new DataBaseConnection();
        PreparedStatement  stmt;
        ResultSet          results;
        Connection         connection;
        long               balancePlayer;
        long               balanceSeller;
        try {
            connection = dbConnection.getConnection();
        }
        catch (SQLException e) {
            e.printStackTrace();
            if (spongeUtilities.getDebugList().contains(player.getUniqueId())) {
                spongeUtilities.debugInChatMessage(player, Text.of(e.getLocalizedMessage()));
            }
            if (spongeUtilities.getDebugList().contains(seller.getUniqueId())) {
                spongeUtilities.debugInChatMessage(seller, Text.of(e.getLocalizedMessage()));
            }
            return false;
        }
        try {
            results = connection.prepareStatement(Constants.getUserMoney(player.getUniqueId())).executeQuery();
            results.first();
            balancePlayer = results.getLong("Balance");
            if (balancePlayer < money) {
                player.sendMessage(Text.of(
                        TextColors.RED,
                        "You doesn't have enough money to pay this person, You hare missing ${} !",
                        money - balancePlayer
                ));
                return false;
            }
            stmt = connection.prepareStatement(Constants.getUserMoney(seller.getUniqueId()));
            results = stmt.executeQuery();
            results.first();
            balanceSeller = results.getLong("Balance");
        }
        catch (SQLException e) {
            e.printStackTrace();
            if (spongeUtilities.getDebugList().contains(player.getUniqueId())) {
                spongeUtilities.debugInChatMessage(player, Text.of(e.getLocalizedMessage()));
            }
            if (spongeUtilities.getDebugList().contains(seller.getUniqueId())) {
                spongeUtilities.debugInChatMessage(seller, Text.of(e.getLocalizedMessage()));
            }
            try {
                connection.close();
            }
            catch (SQLException e1) {
                e1.printStackTrace();
            }
            return false;
        }
        try {
            connection.prepareStatement(Constants.getUpdateURLBalance(
                    balancePlayer - money,
                    player.getUniqueId()
            )).executeQuery();
            connection.prepareStatement(Constants.getUpdateURLBalance(
                    balanceSeller + money,
                    seller.getUniqueId()
            )).executeQuery();
        }
        catch (SQLException e) {
            e.printStackTrace();
            if (spongeUtilities.getDebugList().contains(player.getUniqueId())) {
                spongeUtilities.debugInChatMessage(player, Text.of(e.getLocalizedMessage()));
            }
            if (spongeUtilities.getDebugList().contains(seller.getUniqueId())) {
                spongeUtilities.debugInChatMessage(seller, Text.of(e.getLocalizedMessage()));
            }
            try {
                connection.close();
            }
            catch (SQLException e1) {
                e1.printStackTrace();
            }
            return false;
        }
        try {
            connection.close();
        }
        catch (SQLException e1) {
            e1.printStackTrace();
        }
        return true;
    }

    public Map<UUID, Long> getBalanceMap() {
        return tempBalanceMap;
    }

    private Path getAccountFile() {
        return this.configPath.toAbsolutePath().normalize().resolve("account.json");
    }

    public static Economy_Utilities getPluginInstance() {
        return pluginInstance;
    }

}
